var searchTimeoutId = 0;

function findAncestor (el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
}


function doSearch() {
    var searchTerm = document.getElementById("search-me").value.toLowerCase();

    links = document.querySelectorAll("section a");
    var nbResults = 0;
    links.forEach(function(link) {
	var closest = link.closest("li.doc-container");
	if (link.text.toLowerCase().indexOf(searchTerm) !== -1) {
	    nbResults++;
	    closest.style.display = "block";
	}
	else
	    closest.style.display = "none";
    });

    var txt = nbResults + " fichiers trouvés";
    document.getElementById("number-results").innerText = txt;

    // var goodLinks =  Array.prototype.filter.call(links, function(elt) {
    // 	return elt.text.toLowerCase().indexOf(searchTerm) !== -1;
    // });
    // console.log(goodLinks.length);
}

function doFilter(cid) {
    links = document.querySelectorAll("section a");
    links.forEach(function(link) {
	var closest = link.closest("li.doc-container");
	var current_cid = closest.dataset.cid;
	if (current_cid == cid || cid < 0)
	    closest.style.display = "block";
	else
	    closest.style.display = "none";
    });
}

function initControllers() {
    var searchInput = document.getElementById("search-me");
    searchInput.addEventListener("keyup", function (event) {
	clearTimeout(searchTimeoutId);
	searchTimeoutId = setTimeout(doSearch, 500);
	var searchTerm = searchInput.value;
	console.log(searchTerm);
    });

    var categorySelectors = document.querySelectorAll("#main-menu a");
    categorySelectors.forEach(function(category) {
	category.addEventListener("click", function(event) {
	    var target = event.target;
	    categorySelectors.forEach(function(c) {
		if (c.classList.contains("selected") && c!= target)
		    c.classList.remove("selected");
	    });
	    target.classList.add("selected");
	    doFilter(target.dataset.id);
	});
    });

}

document.addEventListener("DOMContentLoaded", function(event) {
    initControllers();
});
